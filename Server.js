const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.use(
  session({
    resave: false, // don't save the session if unmodified
    saveUninitialized: false, // don't create a session until something is stored
    secret: 'superdupersecretkey',
  }),
);

app.get('/api', (req, res) => {
  res.send({title: 'Aspire'});
});

app.get('/api/user', (req, res) => {
  res.send({
    name: 'Mark Henry',
    balance: 3000,
    card: {
      number: 1234567890123456,
      expiry: '12/25',
      cvv: 455,
    },
    spendingLimit: {
      enabled: req.session.isSpendingLimited ?? false,
      limit: req.session.spendingLimit ?? 0,
    },
  });
});

app.get('/api/spending-limit', (req, res) => {
  res.send({spendingLimit: req.session?.spendingLimit ?? 0});
});

app.post('/api/spending-limit', (req, res) => {
  req.session.spendingLimit = parseFloat(req.body.spendingLimit) || 0;
  req.session.isSpendingLimited = req.session.spendingLimit > 0;
  res.send({
    enabled: req.session.isSpendingLimited,
    limit: req.session.spendingLimit,
  });
});

app.put('/api/spending-limit', (req, res) => {
  req.session.spendingLimit =
    parseFloat(req.body.spendingLimit.limit) || req.session.spendingLimit;
  req.session.isSpendingLimited =
    req.body.spendingLimit.enabled ?? req.session.spendingLimit > 0;
  res.send({
    enabled: req.session.isSpendingLimited,
    limit: req.session.spendingLimit,
  });
});

/**
 * Error Handler
 */
app.use(function (err, _, res, __) {
  res.status(err.status || 500);
  res.send({error: err.message});
});

/**
 * Fallback if no route is found
 */
app.use(function (_, res) {
  res.status(404);
  res.send({error: "Sorry, can't find that"});
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
