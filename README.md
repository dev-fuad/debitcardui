# Debit Card

This is an example of debit card UI for managing personal finance.

![Demo](./Demo.gif)

## Tasks

- As a Debit Card user, I want to be able to see my Debit Card info including available balance, card number, expiry date, cvv, etc.
- As a Debit Card user, I should be able to show or hide sensitive info on Debit Card.
- As a Debit Card user, I should be able to set a weekly limit for my spendings.

## Setup

1. Clone this project
2. Install dependency using command:
  ```shell
  $ yarn
  ```
3. For iOS install Pods using command
  ```shell
  $ npx pod-install
  ```
or
  ```shell
  $ cd ios/ && pod install
  ```
4. For starting server run
  ```shell
  $ yarn server
  ```
5. For running app on iOS or  Android
  ```shell
  $ yarn ios
  ```
or
  ```shell
  $ yarn android
  ```

## Libraries Used
For Client App
1. _React Navigation_ - For Routing / Navigating between screens.
2. _Redux_ with _Redux-Saga_ - For managing state.
3. _toggle-switch-react-native_ - For displaying toggle switch.

For Server
1. _Express_ - For easily making Rest APIs.
2. _Body-Parser_ - For parsing body sent via API.
3. _Express-Session_ - For storing data in session.