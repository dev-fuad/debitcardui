export default class Format {
  static cardNumber(cardNumber: string | number, shouldRedact = false): string {
    const format = shouldRedact ? '••••   ••••   ••••   $4' : '$1  $2  $3  $4';
    return cardNumber
      .toString()
      .replace(/(\d{4})(\d{4})(\d{4})(\d{4})/g, format);
  }

  static currency(amount: number): string {
    return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
}
