/**
 * DebitCardUI
 * api.ts
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {SpendingLimit, User} from '../types';

const BASE_URL = 'http://localhost:3000/api';

export const getUser = async () => {
  const response = await fetch(`${BASE_URL}/user`);
  return response.json() as Promise<User>;
};

export const setSpendingLimit = async (spendingLimit: number) => {
  const response = await fetch(`${BASE_URL}/spending-limit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({spendingLimit}),
  });
  return response.json() as Promise<SpendingLimit>;
};

export const toggleSpendingLimit = async (enabled: boolean) => {
  const response = await fetch(`${BASE_URL}/spending-limit`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({spendingLimit: {enabled}}),
  });
  return response.json() as Promise<SpendingLimit>;
};
