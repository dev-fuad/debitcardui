/**
 * DebitCardUI
 * navigation.ts
 * created: 03/08/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {createNavigationContainerRef} from '@react-navigation/native';
import type {AppStackParamList} from '../types';

export const navigationRef = createNavigationContainerRef<AppStackParamList>();

export const navigate = (name: keyof AppStackParamList, params?: any) => {
  if (navigationRef.isReady()) {
    navigationRef.navigate(name, params);
  }
};
