/**
 * DebitCardUI
 * index.ts
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {takeLatest, all, takeEvery} from 'redux-saga/effects';
import {GET_USER, SET_SPENDING_LIMIT} from '../store/actions/types';
import {getUser, setSpendingLimit} from './UserSaga';

export default function* rootSaga() {
  yield all([
    takeLatest(GET_USER, getUser),
    takeEvery(SET_SPENDING_LIMIT, setSpendingLimit),
  ]);
}
