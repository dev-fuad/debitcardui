/**
 * DebitCardUI
 * UserSaga.ts
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {call, put} from 'redux-saga/effects';
import {API, GlobalNavigation} from '../services';
import {UserActions} from '../store/actions';
import type {SpendingLimit, User, UserAction} from '../types';

export function* getUser() {
  try {
    const user: User = yield call(API.getUser);
    yield put(UserActions.getUserSuccess(user));
  } catch (error: any) {
    yield put(UserActions.apiError(error?.message ?? 'Something went wrong'));
  }
}

export function* setSpendingLimit(action: UserAction) {
  try {
    const isLimit = typeof action.payload === 'number';
    const limit = isLimit
      ? (action.payload as number)
      : (action.payload as boolean);
    const spendingLimit: SpendingLimit = yield call(
      isLimit ? API.setSpendingLimit : API.toggleSpendingLimit,
      limit,
    );
    yield put(UserActions.spendingLimitSuccess(spendingLimit));
    isLimit && GlobalNavigation.navigate('App');
  } catch (error: any) {
    yield put(UserActions.apiError(error?.message ?? 'Something went wrong'));
  }
}
