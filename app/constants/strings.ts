export const Strings = {
  debitCard: {
    debitCard: 'Debit Card',
    availableBalance: 'Available balance',
    hideCardNumber: 'Hide card number',
    showCardNumber: 'Show card number',
    thru: 'Thru: ',
    cvv: 'CVV: ',
    cardSpendingLimit: 'Debit card spending limit',
    topupAccount: 'Top-up account',
    topupAccountInfo: 'Deposit money to your account to use with card',
    weeklySpendingLimit: 'Weekly spending limit',
    weeklySpendingLimitInfo: 'You haven’t set any spending limit on card',
    weeklySpendingLimitAmount: 'Your weekly spending limit is S$ {amount}',
    freezeCard: 'Freeze card',
    freezeCardInfo: 'Your debit card is currently active',
    getNewCard: 'Get a new card',
    getNewCardInfo: 'This deactivates your current debit card',
    deactivatedCards: 'Deactivated cards',
    deactivatedCardsInfo: 'Your previously deactivated cards',
  },
  spendingLimit: {
    spendingLimit: 'Spending limit',
    setSpendingLimit: 'Set a weekly debit card spending limit',
    weeklyNote: 'Here weekly means the last 7 days - not the calendar week',
    save: 'Save',
  },
};
