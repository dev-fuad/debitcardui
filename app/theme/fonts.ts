import {Font} from '../assets';
import {Color, pallete} from './colors';

export const typography = {
  heading: {
    fontFamily: Font.avenirNext.bold,
    fontSize: 24,
    color: pallete.white,
  },
  title: {
    fontFamily: Font.avenirNext.bold,
    fontSize: 22,
    color: pallete.white,
    lineHeight: 24,
  },
  body: {
    fontFamily: Font.avenirNext.medium,
    fontSize: 14,
    color: Color.text,
  },
  button: {
    fontFamily: Font.avenirNext.demi,
    fontSize: 16,
    color: pallete.white,
  },
  action: {
    fontFamily: Font.avenirNext.demi,
    fontSize: 12,
    color: Color.primary,
  },
  cardNumber: {
    fontFamily: Font.avenirNext.demi,
    fontSize: 14,
    color: pallete.white,
    lineHeight: 16,
  },
  expiry: {
    fontFamily: Font.avenirNext.demi,
    fontSize: 13,
    color: pallete.white,
  },
  sub: {
    fontFamily: Font.avenirNext.regular,
    fontSize: 13,
    color: Color.subText,
  },
  progress: {
    fontFamily: Font.avenirNext.medium,
    fontSize: 13,
    color: pallete.greyDark,
  },
};
