export const pallete = {
  greenPrimary: '#01D167',
  greenPrimary7: '#01D16712',
  greenPrimary10: '#01D1671A',

  bluePrimary: '#0C365A',
  blueDark: '#25345F',

  greyLightest: '#E5E5E5',
  greyLighter: '#EEEEEE',
  greyLight: '#DDDDDD',
  greyDark: '#222222',
  greyDark40: '#22222266',

  white: '#FFFFFF',
  black: '#000000',
  transparent: '#00000000',
};

export const Color = {
  primary: pallete.greenPrimary,
  secondary: pallete.bluePrimary,

  background: pallete.white,

  text: pallete.blueDark,
  subText: pallete.greyDark40,

  tabbarActive: pallete.greenPrimary,
  tabbarInactive: pallete.greyLight,

  disabled: pallete.greyLighter,
  border: pallete.greyLightest,
};
