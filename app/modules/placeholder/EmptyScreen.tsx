/**
 * DebitCardUI
 * EmptyScreen.tsx
 * created: 03/05/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import {SafeAreaView, Text, StyleSheet} from 'react-native';
import {typography} from '../../theme';

const EmptyScreen: React.FC = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Text style={typography.heading}>Comming Soon...</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default EmptyScreen;
