/**
 * DebitCardUI
 * Pill.tsx
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import {Dimensions, Pressable, StyleSheet, Text, View} from 'react-native';
import {Format} from '../../services';
import {pallete, typography} from '../../theme';

interface PillsProps {
  limits: number[];
  onSelect: (limit: number) => void;
}

interface PillProps {
  amount: number;
  onPress: () => void;
}

const Pill: React.FC<PillProps> = ({amount, onPress}) => {
  return (
    <Pressable style={styles.pill} onPress={onPress}>
      <Text style={styles.label}>S$ {Format.currency(amount)}</Text>
    </Pressable>
  );
};

const Pills: React.FC<PillsProps> = ({limits, onSelect}) => {
  return (
    <View style={styles.container}>
      {limits.map(limit => (
        <Pill
          key={limit.toString()}
          amount={limit}
          onPress={() => onSelect(limit)}
        />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginVertical: 32,
    marginHorizontal: 24,
  },
  pill: {
    backgroundColor: pallete.greenPrimary7,
    height: 40,
    // 24 + 12 + 12 + 24 = 72
    width: (Dimensions.get('window').width - 72) / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    ...typography.action,
  },
});

export default Pills;
