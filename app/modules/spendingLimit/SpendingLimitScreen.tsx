/**
 * DebitCardUI
 * SpendingLimitScreen.tsx
 * created: 03/05/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React, {useCallback, useState} from 'react';
import {Image, Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Icon} from '../../assets';
import {BottomSheet, Button, Navbar, TextInput} from '../../components';
import {Spinner} from '../../components';
import {Strings} from '../../constants';
import {useAppDispatch, useAppSelector} from '../../store';
import {UserActions} from '../../store/actions';
import type {AppStackScreenProps} from '../../types';
import Pills from './Pills';
import styles from './styles';

interface SpendingLimitScreenProps
  extends AppStackScreenProps<'SpendingLimit'> {}

const defaultLimitOptions = [5000, 10000, 20000];

const SpendingLimitScreen: React.FC<SpendingLimitScreenProps> = () => {
  const dispatch = useAppDispatch();
  const limit = useAppSelector(state => state.spendingLimit?.limit);
  const fetching = useAppSelector(state => state.fetching);
  const [spendingLimit, setSpendingLimit] = useState<string>(
    limit?.toString() ?? '',
  );

  const onSave = useCallback(() => {
    const parsedLimit = parseInt(spendingLimit, 10);
    if (isNaN(parsedLimit)) {
      return;
    }
    dispatch(UserActions.setSpendingLimit(parsedLimit));
  }, [dispatch, spendingLimit]);

  return (
    <View style={styles.container}>
      <Navbar title={Strings.spendingLimit.spendingLimit} />
      <BottomSheet style={styles.bottomSheet}>
        <View style={styles.row}>
          <Image style={styles.spendingLimitIcon} source={Icon.spendingLimit} />
          <Text style={styles.spendingLimitText}>
            {Strings.spendingLimit.setSpendingLimit}
          </Text>
        </View>
        <TextInput
          containerStyle={styles.input}
          keyboardType="decimal-pad"
          value={spendingLimit}
          onChangeText={setSpendingLimit}
        />
        <Text style={styles.weeklyNote}>
          {Strings.spendingLimit.weeklyNote}
        </Text>
        <Pills
          limits={defaultLimitOptions}
          onSelect={value => setSpendingLimit(value.toString())}
        />
        <View style={styles.spacer} />
        <SafeAreaView edges={['bottom']}>
          <Button
            style={styles.button}
            title={Strings.spendingLimit.save}
            onPress={onSave}
          />
        </SafeAreaView>
      </BottomSheet>
      <Spinner visible={fetching} />
    </View>
  );
};

export default SpendingLimitScreen;
