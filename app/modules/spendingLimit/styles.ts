/**
 * DebitCardUI
 * styles.ts
 * created: 03/06/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {StyleSheet} from 'react-native';
import {pallete, typography} from '../../theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bottomSheet: {
    marginTop: 40,
    paddingTop: 32,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  spendingLimitIcon: {
    marginLeft: 24,
    marginRight: 12,
  },
  spendingLimitText: {
    ...typography.body,
    color: pallete.greyDark,
  },
  input: {
    marginHorizontal: 24,
    marginVertical: 12,
  },
  weeklyNote: {
    ...typography.sub,
    marginHorizontal: 24,
  },
  spacer: {
    flex: 1,
  },
  button: {
    marginHorizontal: '10%',
    marginBottom: 24,
  },
});

export default styles;
