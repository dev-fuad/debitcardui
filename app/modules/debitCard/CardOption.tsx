/**
 * DebitCardUI
 * CardOption.tsx
 * created: 03/06/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import {Image, Text, View} from 'react-native';
import Switch from 'toggle-switch-react-native';
import type {CardOptionType} from '../../types';
import styles from './styles';

interface CardOptionProps {
  option: CardOptionType;
  index: number;
  onToggle?: (checked: boolean, index: number) => void;
}

const CardOption: React.FC<CardOptionProps> = ({option, index, onToggle}) => {
  return (
    <View style={styles.optionItem}>
      <Image source={option.icon} style={styles.cardOptionItemIcon} />
      <View style={styles.container}>
        <Text style={styles.cardOptionItemTitle}>{option.title}</Text>
        <Text style={styles.cardOptionItemInfo}>{option.info}</Text>
      </View>
      {option.checked !== undefined && (
        <Switch
          size="small"
          isOn={option.checked}
          onToggle={isOn => onToggle?.(isOn, index)}
        />
      )}
    </View>
  );
};

export default CardOption;
