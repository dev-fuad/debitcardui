import {Icon} from '../../assets';
import {Strings} from '../../constants';
import type {CardOptionType} from '../../types';

export const CardOptions: CardOptionType[] = [
  {
    icon: Icon.topup,
    title: Strings.debitCard.topupAccount,
    info: Strings.debitCard.topupAccountInfo,
  },
  {
    icon: Icon.spending,
    title: Strings.debitCard.weeklySpendingLimit,
    info: Strings.debitCard.weeklySpendingLimitInfo,
    checked: false,
  },
  {
    icon: Icon.freeze,
    title: Strings.debitCard.freezeCard,
    info: Strings.debitCard.freezeCardInfo,
    checked: false,
  },
  {
    icon: Icon.newCard,
    title: Strings.debitCard.getNewCard,
    info: Strings.debitCard.getNewCardInfo,
  },
  {
    icon: Icon.deactivate,
    title: Strings.debitCard.deactivatedCards,
    info: Strings.debitCard.deactivatedCardsInfo,
  },
];
