/**
 * DebitCardUI
 * DebitCardScreen.tsx
 * created: 03/05/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import {FlatList, Text, View} from 'react-native';
import {BottomSheet, CurrencySymbol, DebitCard, Navbar} from '../../components';
import {Strings} from '../../constants';
import {Format} from '../../services';
import CardOption from './CardOption';
import useDebitCardController from './DebitCardController';
import ProgressBar from './ProgressBar';
import styles from './styles';

const DebitCardScreen: React.FC = () => {
  const {availableBalance, cardOptions, onToggle} = useDebitCardController();

  return (
    <View style={styles.container}>
      <Navbar title={Strings.debitCard.debitCard} />
      <Text style={styles.balanceLabel}>
        {Strings.debitCard.availableBalance}
      </Text>
      <View style={styles.balanceAmount}>
        <CurrencySymbol />
        <Text style={styles.balanceValue}>
          {Format.currency(availableBalance)}
        </Text>
      </View>
      <BottomSheet style={styles.bottomSheet}>
        <FlatList
          data={cardOptions}
          style={styles.cardOptions}
          contentContainerStyle={styles.cardOptionsContent}
          renderItem={({item, index}) => (
            <CardOption option={item} index={index} onToggle={onToggle} />
          )}
        />
        <ProgressBar />
        <DebitCard style={styles.card} />
      </BottomSheet>
    </View>
  );
};

export default DebitCardScreen;
