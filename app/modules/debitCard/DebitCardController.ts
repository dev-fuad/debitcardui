/**
 * DebitCardUI
 * DebitCardController.ts
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {useNavigation} from '@react-navigation/native';
import {useCallback, useEffect, useState} from 'react';
import {LayoutAnimation, Platform, UIManager} from 'react-native';
import {useAppDispatch, useAppSelector} from '../../store';
import {UserActions} from '../../store/actions';
import {CardOptionType, TabScreenProps} from '../../types';
import {CardOptions} from './CardOptions';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

const useDebitCardController = () => {
  const navigation = useNavigation<TabScreenProps<'DebitCard'>['navigation']>();
  const dispatch = useAppDispatch();
  const availableBalance = useAppSelector(state => state.balance);
  const isSpendingLimitEnabled = useAppSelector(
    state => state.spendingLimit?.enabled,
  );
  const [cardOptions, setCardOptions] = useState<CardOptionType[]>(CardOptions);

  // For smooth transition between hide/show of progress bar
  LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

  const onToggle = useCallback(
    (isOn: boolean, index: number) => {
      setCardOptions(currentOptions => {
        if (index === 1 && currentOptions[index].checked === false) {
          navigation.navigate('SpendingLimit');
          return currentOptions;
        } else if (index === 1) {
          dispatch(UserActions.setSpendingLimit(false));
        }
        const newOptions = currentOptions.map((option, idx) => ({
          ...option,
          checked: index === idx ? isOn : option.checked,
        }));
        return newOptions;
      });
    },
    [dispatch, navigation],
  );

  useEffect(() => {
    if (isSpendingLimitEnabled !== undefined) {
      setCardOptions(currentOptions => {
        const newOptions = currentOptions.map((option, index) => ({
          ...option,
          checked: index === 1 ? isSpendingLimitEnabled : option.checked,
        }));
        return newOptions;
      });
    }
  }, [isSpendingLimitEnabled, onToggle]);

  useEffect(() => {
    dispatch(UserActions.getUser());
  }, [dispatch]);

  return {
    availableBalance,
    cardOptions,
    onToggle,
  };
};

export default useDebitCardController;
