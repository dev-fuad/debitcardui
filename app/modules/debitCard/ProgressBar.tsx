/**
 * DebitCardUI
 * ProgressBar.tsx
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Strings} from '../../constants';
import {useAppSelector} from '../../store';
import {Color, pallete, typography} from '../../theme';

interface ProgressBarProps {}

const ProgressBar: React.FC<ProgressBarProps> = () => {
  const spendingLimit = useAppSelector(state => state.spendingLimit);
  const spends = 1345;

  if (!spendingLimit?.enabled) {
    return null;
  }
  const width = `${(spends / spendingLimit.limit) * 100}%`;

  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <Text style={styles.label}>{Strings.debitCard.cardSpendingLimit}</Text>
        <Text style={styles.amountSpent}>${spends}</Text>
        <View style={styles.separator} />
        <Text style={styles.totalAmount}>${spendingLimit.limit}</Text>
      </View>
      <View style={styles.track}>
        <View style={[styles.progress, {width}]} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 24,
    marginBottom: 0,
  },
  track: {
    backgroundColor: pallete.greenPrimary10,
    width: '100%',
    height: 15,
    borderRadius: 30,
    overflow: 'hidden',
  },
  progress: {
    backgroundColor: Color.primary,
    height: '100%',
    transform: [{skewX: '-18deg'}],
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    ...typography.progress,
    flex: 1,
    marginBottom: 6,
  },
  amountSpent: {
    ...typography.expiry,
    color: Color.primary,
  },
  separator: {
    width: 1,
    height: '50%',
    backgroundColor: pallete.greyDark,
    marginHorizontal: 6,
    opacity: 0.2,
  },
  totalAmount: {
    ...typography.progress,
    opacity: 0.2,
  },
});

export default ProgressBar;
