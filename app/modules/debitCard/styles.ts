/**
 * DebitCardUI
 * styles.ts
 * created: 03/05/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {StyleSheet} from 'react-native';
import {Color, pallete, typography} from '../../theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  balanceLabel: {
    ...typography.body,
    color: pallete.white,
    margin: 24,
    marginBottom: 10,
  },
  balanceAmount: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 24,
  },
  balanceValue: {
    ...typography.heading,
    marginLeft: 8,
  },
  currencySignContainer: {
    backgroundColor: Color.primary,
    borderRadius: 4,
    paddingVertical: 3,
    paddingHorizontal: 12,
  },
  currencySign: {
    ...typography.heading,
    fontSize: 12,
  },
  bottomSheet: {
    marginTop: 80,
    flexDirection: 'column-reverse',
  },
  card: {
    marginTop: -80,
  },
  cardOptions: {
    marginTop: -8,
  },
  cardOptionsContent: {
    paddingTop: 8,
  },
  optionItem: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding: 24,
    paddingBottom: 8,
  },
  cardOptionItemIcon: {
    marginRight: 12,
  },
  cardOptionItemTitle: {
    ...typography.body,
  },
  cardOptionItemInfo: {
    ...typography.sub,
  },
  cardOptionItemSwitch: {
    height: 20,
    width: 34,
  },
});

export default styles;
