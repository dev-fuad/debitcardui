const Font = {
  avenirNext: {
    regular: 'AvenirNext-Regular',
    medium: 'AvenirNext-Medium',
    bold: 'AvenirNext-Bold',
    demi: 'AvenirNext-DemiBold',
  },
};

export default Font;
