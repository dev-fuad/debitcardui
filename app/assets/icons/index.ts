const Icon = {
  logo: require('./logo/logo.png'),
  back: require('./back/back.png'),

  home: require('./tabIcons/Home/Home.png'),
  debitCard: require('./tabIcons/DebitCard/DebitCard.png'),
  payments: require('./tabIcons/Payments/Payments.png'),
  credit: require('./tabIcons/Credit/Credit.png'),
  account: require('./tabIcons/Account/Account.png'),

  aspireLogo: require('./cardUI/aspireLogo/aspireLogo.png'),
  visaLogo: require('./cardUI/visaLogo/visaLogo.png'),

  eyeClosed: require('./cardUI/eyeClosed/eyeClosed.png'),
  eyeOpened: require('./cardUI/eyeOpened/eyeOpened.png'),

  topup: require('./debitCard/topup/topup.png'),
  spending: require('./debitCard/spending/spending.png'),
  freeze: require('./debitCard/freeze/freeze.png'),
  newCard: require('./debitCard/newCard/newCard.png'),
  deactivate: require('./debitCard/deactivate/deactivate.png'),

  spendingLimit: require('./spendingLimit/spendingLimit.png'),
};

export default Icon;
