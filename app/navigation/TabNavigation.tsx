/**
 * DebitCardUI
 * TabNavigation.tsx
 * created: 03/05/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {Image} from 'react-native';
import {Icon} from '../assets';
import {Strings} from '../constants';
import {DebitCardScreen} from '../modules/debitCard';
import {EmptyScreen} from '../modules/placeholder';
import {Color} from '../theme';
import type {TabsParamList} from '../types';

const Tab = createBottomTabNavigator<TabsParamList>();

const IconMap = {
  Home: Icon.home,
  DebitCard: Icon.debitCard,
  Payments: Icon.payments,
  Credit: Icon.credit,
  Profile: Icon.account,
} as {[key: string]: any};

const TabNavigation = () => {
  return (
    <Tab.Navigator
      backBehavior="none"
      sceneContainerStyle={{backgroundColor: Color.secondary}}
      screenOptions={({route}) => ({
        headerShown: false,
        tabBarIcon: ({color}) => {
          let icon;
          icon = IconMap[route.name];

          return <Image style={{tintColor: color}} source={icon} />;
        },
        tabBarActiveTintColor: Color.tabbarActive,
        tabBarInactiveTintColor: Color.tabbarInactive,
      })}>
      <Tab.Screen name="Home" component={EmptyScreen} />
      <Tab.Screen
        name="DebitCard"
        options={{title: Strings.debitCard.debitCard}}
        component={DebitCardScreen}
      />
      <Tab.Screen name="Payments" component={EmptyScreen} />
      <Tab.Screen name="Credit" component={EmptyScreen} />
      <Tab.Screen name="Profile" component={EmptyScreen} />
    </Tab.Navigator>
  );
};

export default TabNavigation;
