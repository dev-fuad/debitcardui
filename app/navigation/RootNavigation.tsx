/**
 * DebitCardUI
 * RootNavigation.tsx
 * created: 03/05/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {SpendingLimitScreen} from '../modules/spendingLimit';
import {GlobalNavigation} from '../services';
import {Color} from '../theme';
import type {AppStackParamList} from '../types';
import TabNavigation from './TabNavigation';

const Stack = createNativeStackNavigator<AppStackParamList>();

const AppNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        contentStyle: {backgroundColor: Color.secondary},
      }}>
      <Stack.Screen name="App" component={TabNavigation} />
      <Stack.Screen name="SpendingLimit" component={SpendingLimitScreen} />
    </Stack.Navigator>
  );
};

const RootNavigation: React.FC = () => {
  return (
    <NavigationContainer ref={GlobalNavigation.navigationRef}>
      <AppNavigation />
    </NavigationContainer>
  );
};

export default RootNavigation;
