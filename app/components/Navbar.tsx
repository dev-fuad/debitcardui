/**
 * DebitCardUI
 * Navbar.tsx
 * created: 03/06/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Icon} from '../assets';
import {Color, typography} from '../theme';

interface NavbarProps {
  title: string;
}

const Navbar: React.FC<NavbarProps> = ({title}) => {
  const navigation = useNavigation();
  const canGoBack = navigation.canGoBack();
  const goBack = () => navigation.goBack();
  return (
    <SafeAreaView style={styles.container} edges={['top']}>
      <View style={styles.titleContainer}>
        {canGoBack && (
          <Pressable style={styles.backButton} onPress={goBack}>
            <Image source={Icon.back} style={styles.backIcon} />
          </Pressable>
        )}
        <Text style={styles.title}>{title}</Text>
      </View>
      <Image style={styles.logo} source={Icon.logo} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
  },
  titleContainer: {
    flex: 1,
  },
  title: {
    ...typography.heading,
    marginHorizontal: 24,
    marginTop: 20,
  },
  backButton: {
    alignSelf: 'flex-start',
  },
  backIcon: {
    tintColor: Color.background,
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginVertical: 16,
    marginHorizontal: 24,
  },
  logo: {
    margin: 16,
    marginHorizontal: 24,
  },
});

export default Navbar;
