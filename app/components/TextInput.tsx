/**
 * DebitCardUI
 * TextInput.tsx
 * created: 03/06/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import type {
  StyleProp,
  TextInputProps as RNTextInputProps,
  ViewStyle,
} from 'react-native';
import {StyleSheet, TextInput as RNTextInput, View} from 'react-native';
import {Color, pallete, typography} from '../theme';
import CurrencySymbol from './CurrencySymbol';

interface TextInputProps extends RNTextInputProps {
  containerStyle?: StyleProp<ViewStyle>;
}

const TextInput: React.FC<TextInputProps> = ({
  style,
  containerStyle,
  ...props
}) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <CurrencySymbol />
      <RNTextInput style={[styles.input, style]} {...props} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: Color.border,
    borderBottomWidth: StyleSheet.hairlineWidth,
    paddingVertical: 10,
  },
  input: {
    ...typography.heading,
    marginLeft: 10,
    color: pallete.greyDark,
    flex: 1,
  },
});

export default TextInput;
