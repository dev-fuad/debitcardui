/**
 * DebitCardUI
 * Spinner.tsx
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import {ActivityIndicator, StyleSheet} from 'react-native';
import {Color} from '../theme';

interface SpinnerProps {
  visible: boolean;
}

const Spinner: React.FC<SpinnerProps> = ({visible}) => {
  if (!visible) {
    return null;
  }
  return (
    <ActivityIndicator
      style={StyleSheet.absoluteFill}
      color={Color.primary}
      size="large"
    />
  );
};

export default Spinner;
