import {Dimensions, StyleSheet} from 'react-native';
import {Color, pallete, typography} from '../../theme';

const styles = StyleSheet.create({
  container: {
    shadowColor: pallete.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    marginHorizontal: 24,
    alignItems: 'flex-end',
    backgroundColor: pallete.transparent,
    overflow: 'visible',
  },
  hideNumber: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Color.background,
    borderRadius: 6,
    padding: 8,
    marginBottom: -20,
    paddingBottom: 24,
    elevation: 4,
  },
  hideNumberText: {
    ...typography.action,
    marginLeft: 8,
  },
  card: {
    backgroundColor: Color.primary,
    width: Dimensions.get('window').width - 48,
    aspectRatio: 1.66,
    borderRadius: 12,
    elevation: 4,
  },
  aspireLogo: {
    margin: 24,
    alignSelf: 'flex-end',
  },
  username: {
    ...typography.title,
    marginHorizontal: 24,
    fontSize: 22,
  },
  cardNumber: {
    ...typography.cardNumber,
    marginTop: 20,
    marginHorizontal: 24,
    letterSpacing: 3.46,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  expiry: {
    ...typography.expiry,
    marginLeft: 24,
    marginRight: 32,
    marginTop: 14,
  },
  cvv: {
    ...typography.expiry,
    marginTop: 14,
    letterSpacing: 1.5,
  },
  cvvRedacted: {
    ...typography.expiry,
    fontSize: 24,
    letterSpacing: 2.8,
    marginLeft: 0,
    marginBottom: -21,
  },
  visaLogo: {
    alignSelf: 'flex-end',
    margin: 24,
    marginTop: 4,
  },
});

export default styles;
