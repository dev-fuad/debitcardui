/**
 * DebitCardUI
 * DebitCard.tsx
 * created: 03/05/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React, {useState} from 'react';
import {ViewProps} from 'react-native';
import {Image, Text, View} from 'react-native';
import {Icon} from '../../assets';
import {Strings} from '../../constants';
import {Format} from '../../services';
import {useAppSelector} from '../../store';
import HideNumberCard from './HideNumberCard';
import styles from './styles';

interface DebitCardProps {
  style?: ViewProps['style'];
}

const DebitCard: React.FC<DebitCardProps> = ({style}) => {
  const name = useAppSelector(state => state.name);
  const card = useAppSelector(state => state.card);
  const [showNumber, setShowNumber] = useState<boolean>(false);

  return (
    <View style={[styles.container, style]}>
      <HideNumberCard showNumber={showNumber} setShowNumber={setShowNumber} />
      <View style={styles.card}>
        <Image style={styles.aspireLogo} source={Icon.aspireLogo} />
        <Text style={styles.username}>{name}</Text>
        <Text style={styles.cardNumber}>
          {Format.cardNumber(card?.number ?? 0, !showNumber)}
        </Text>
        <View style={styles.row}>
          <Text style={styles.expiry}>
            {Strings.debitCard.thru}
            {card?.expiry}
          </Text>
          <View style={styles.row}>
            <Text style={styles.cvv}>{Strings.debitCard.cvv}</Text>
            <Text style={showNumber ? styles.cvv : styles.cvvRedacted}>
              {showNumber ? card?.cvv : '***'}
            </Text>
          </View>
        </View>
        <Image style={styles.visaLogo} source={Icon.visaLogo} />
      </View>
    </View>
  );
};

export default DebitCard;
