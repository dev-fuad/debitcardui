/**
 * DebitCardUI
 * HideNumberCard.tsx
 * created: 03/06/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import type {Dispatch, SetStateAction} from 'react';
import React from 'react';
import {Image, Pressable, Text} from 'react-native';
import {Icon} from '../../assets';
import {Strings} from '../../constants';
import styles from './styles';

interface HideNumberCardProps {
  showNumber: boolean;
  setShowNumber: Dispatch<SetStateAction<boolean>>;
}

const HideNumberCard: React.FC<HideNumberCardProps> = ({
  showNumber,
  setShowNumber,
}) => {
  return (
    <Pressable
      style={styles.hideNumber}
      onPress={() => setShowNumber((shown: boolean) => !shown)}>
      <Image source={showNumber ? Icon.eyeClosed : Icon.eyeOpened} />
      <Text style={styles.hideNumberText}>
        {showNumber
          ? Strings.debitCard.hideCardNumber
          : Strings.debitCard.showCardNumber}
      </Text>
    </Pressable>
  );
};

export default HideNumberCard;
