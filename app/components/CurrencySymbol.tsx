/**
 * DebitCardUI
 * CurrencySymbol.tsx
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Color, typography} from '../theme';

interface IProps {}

const CurrencySymbol: React.FC<IProps> = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>S$</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.primary,
    borderRadius: 4,
    paddingVertical: 3,
    paddingHorizontal: 12,
  },
  text: {
    ...typography.heading,
    fontSize: 12,
  },
});

export default CurrencySymbol;
