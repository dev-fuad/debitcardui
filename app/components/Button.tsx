/**
 * DebitCardUI
 * Button.tsx
 * created: 03/06/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import type {StyleProp, TextStyle} from 'react-native';
import {Pressable, StyleSheet, Text} from 'react-native';
import type {PressableProps} from 'react-native';
import {Color, typography} from '../theme';

interface ButtonProps extends PressableProps {
  title?: string;
  titleStyle?: StyleProp<TextStyle>;
}

const Button: React.FC<ButtonProps> = ({
  style,
  title,
  titleStyle,
  children,
  disabled,
  ...props
}) => {
  return (
    <Pressable
      style={({pressed}) => [
        styles.container,
        disabled && styles.disabled,
        typeof style === 'function' ? style({pressed}) : style,
      ]}
      disabled={disabled}
      {...props}>
      <Text style={[styles.title, titleStyle]}>{title}</Text>
      {children}
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.primary,
    maxWidth: '80%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
  },
  disabled: {
    backgroundColor: Color.disabled,
  },
  title: {
    ...typography.button,
  },
});

export default Button;
