/**
 * DebitCardUI
 * BottomSheet.tsx
 * created: 03/05/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import React from 'react';
import {StyleSheet, View} from 'react-native';
import type {ViewProps} from 'react-native';
import {Color} from '../theme';

const BottomSheet: React.FC<ViewProps> = ({style, ...props}: ViewProps) => {
  return <View style={[styles.container, style]} {...props} />;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.background,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
  },
});

export default BottomSheet;
