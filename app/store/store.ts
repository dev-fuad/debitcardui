/**
 * DebitCardUI
 * store.ts
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import {applyMiddleware, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga';
import {UserReducer} from './reducers';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(UserReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;
