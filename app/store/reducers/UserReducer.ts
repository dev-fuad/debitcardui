/**
 * DebitCardUI
 * UserReducer.ts
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import type {ActionType, User, UserAction, UserState} from '../../types';
import {
  GET_USER,
  GET_USER_SUCCESS,
  GET_SPENDING_LIMIT,
  SET_SPENDING_LIMIT,
  SPENDING_LIMIT_SUCCESS,
  API_ERROR,
} from '../actions/types';

const initialState: UserState = {
  name: '',
  balance: 0,
  card: undefined,
  spendingLimit: undefined,
  error: undefined,
  fetching: false,
};

const getUser = (state: UserState) => ({...state, error: null, fetching: true});
const getUserSuccess = (state: UserState, action: UserAction) => {
  return {
    ...state,
    ...(action.payload as User),
    fetching: false,
  };
};

const getSpendingLimit = (state: UserState) => ({
  ...state,
  error: undefined,
  fetching: true,
});
const setSpendingLimit = (state: UserState) => ({
  ...state,
  error: undefined,
  fetching: true,
});
const spendingLimitSuccess = (state: UserState, action: UserAction) => ({
  ...state,
  spendingLimit: action.payload,
  fetching: false,
});

const apiError = (state: UserState, action: UserAction) => ({
  ...state,
  error: action.payload,
  fetching: false,
});

const reducerMap: {[key in ActionType]: Function} = {
  [GET_USER]: getUser,
  [GET_USER_SUCCESS]: getUserSuccess,
  [GET_SPENDING_LIMIT]: getSpendingLimit,
  [SET_SPENDING_LIMIT]: setSpendingLimit,
  [SPENDING_LIMIT_SUCCESS]: spendingLimitSuccess,
  [API_ERROR]: apiError,
};

export default function (state = initialState, action: UserAction) {
  return reducerMap[action.type]?.(state, action) ?? state;
}
