/**
 * DebitCardUI
 * UserActions.ts
 * created: 03/07/2022
 * Fuad Mohd. Firoz
 *
 * @format
 */

import type {SpendingLimit, User, UserAction} from '../../types';
import {
  GET_USER,
  GET_USER_SUCCESS,
  GET_SPENDING_LIMIT,
  SET_SPENDING_LIMIT,
  SPENDING_LIMIT_SUCCESS,
  API_ERROR,
} from './types';

export const getUser = (): UserAction => ({type: GET_USER});

export const getUserSuccess = (user: User): UserAction => ({
  type: GET_USER_SUCCESS,
  payload: user,
});

export const getSpendingLimit = (): UserAction => ({type: GET_SPENDING_LIMIT});
export const setSpendingLimit = (limit: number | boolean): UserAction => ({
  type: SET_SPENDING_LIMIT,
  payload: limit,
});

export const spendingLimitSuccess = (limit: SpendingLimit): UserAction => ({
  type: SPENDING_LIMIT_SUCCESS,
  payload: limit,
});

export const apiError = (error: string): UserAction => ({
  type: API_ERROR,
  payload: error,
});
