import {BottomTabScreenProps} from '@react-navigation/bottom-tabs';
import {
  CompositeScreenProps,
  NavigatorScreenParams,
} from '@react-navigation/native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';

export type TabsParamList = {
  Home: undefined;
  DebitCard: undefined;
  Payments: undefined;
  Credit: undefined;
  Profile: undefined;
};

export type AppStackParamList = {
  App: NavigatorScreenParams<TabsParamList>;
  SpendingLimit: undefined;
};

export type AppStackScreenProps<T extends keyof AppStackParamList> =
  NativeStackScreenProps<AppStackParamList, T>;

export type TabScreenProps<T extends keyof TabsParamList> =
  CompositeScreenProps<
    BottomTabScreenProps<TabsParamList, T>,
    AppStackScreenProps<keyof AppStackParamList>
  >;

declare global {
  namespace ReactNavigation {
    interface RootParamList extends AppStackParamList {}
  }
}
