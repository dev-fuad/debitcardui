import type {Action} from 'redux';

export interface Card {
  number: number;
  expiry: string;
  cvv: number;
}

export interface SpendingLimit {
  limit: number;
  enabled: boolean;
}

export interface User {
  name: string;
  balance: number;
  card?: Card;
  spendingLimit?: SpendingLimit;
}

export type ActionType =
  | 'GET_USER'
  | 'GET_USER_SUCCESS'
  | 'GET_SPENDING_LIMIT'
  | 'SET_SPENDING_LIMIT'
  | 'SPENDING_LIMIT_SUCCESS'
  | 'API_ERROR';

export interface UserState extends User {
  fetching: boolean;
  error?: string;
}

export interface UserAction extends Action<ActionType> {
  payload?: User | SpendingLimit | string | number | boolean;
}
