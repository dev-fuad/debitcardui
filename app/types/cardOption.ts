import type {ImageSourcePropType} from 'react-native';

export type CardOptionType = {
  icon: ImageSourcePropType;
  title: string;
  info: string;
  checked?: boolean;
};
